package com.company;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String habits[];

    static {
        System.out.printf("Загружается новый класс %s\n",Pet.class.getSimpleName());
    }

    {
        System.out.printf("%s: новый экземпляр\n",this.getClass().getSimpleName());
    }

    public Pet(){
        this.species = "undefined";
        this.nickname = "undefined";
        this.age = 0;
        this.trickLevel = 0;
        this.habits = new String[0];
    }

    public Pet(String species, String nickname){
        this();
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits){
        this(species,nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;

    }

    public void setSpecies(String species){
        this.species = species;
    }

    public String getSpecies(){
        return species;
    }

    public void setNickname(String nickname){
        this.nickname = nickname;
    }

    public String getNickname(){
        return nickname;
    }

    public void setAge(int age){
        this.age = age;
    }

    public int getAge(){
        return age;
    }

    public void setTrickLevel(int trickLevel){
        if( trickLevel <= 100 && trickLevel >= 0 ){
            this.trickLevel = trickLevel;
        } else System.out.printf("Trick Level is out of range\n");
    }

    public int getTrickLevel(){
        return trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public String[] getHabits() {
        return habits;
    }

    void eat(){
        System.out.printf("Я кушаю!\n");
    }

    void respond(){
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!\n",getNickname());
    }

    void foul(){
        System.out.printf("Нужно хорошо замести следы...\n");
    }

    @Override
    public String toString() {
        return String.format(getSpecies() + "{" +
                "nickname=" + "'" + getNickname() +
                "', age="+getAge()) +
                ", trickLevel=" + getTrickLevel() +
                ", habits=" + Arrays.toString(getHabits()) +
                "}\n";
    }
}
