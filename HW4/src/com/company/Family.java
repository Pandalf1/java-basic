package com.company;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    static {
        System.out.printf("Загружается новый класс %s\n",Family.class.getSimpleName());
    }

    {
        System.out.printf("%s: новый экземпляр\n",this.getClass().getSimpleName());
    }

    public Family(Human mother, Human father){
        this.mother = mother;
        this.mother.setFamily(this);
        this.father = father;
        this.father.setFamily(this);
        this.children = new Human[0];
        this.pet = null;
    }

    public Family(Human mother, Human father, Human[] children){
        this(mother, father);
        this.children = children;
    }

    public Family(Human mother, Human father, Pet pet){
        this(mother, father);
        this.pet = pet;
    }

    public Family(Human mother, Human father, Human[] children, Pet pet){
        this(mother, father);
        this.children = children;
        this.pet = pet;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getMother() {
        return mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human getFather() {
        return father;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public Pet getPet() {
        return pet;
    }

    void greetPet(){
        System.out.printf("Привет, %s\n", pet.getNickname());
    }

    void describePet(){
        String trickLevel;
        if(pet.getTrickLevel() > 50){
            trickLevel = "очень хитрый";
        } else trickLevel = "почти не хитрый";

        System.out.printf("У нас есть %s, ему %i лет, он %s\n",
                pet.getSpecies(),
                pet.getAge(),
                trickLevel);
    }

    @Override
    public String toString() {
        return String.format("Familiy{mother="+ mother +
                ", father=" + father +
                ", pet=" + pet +
                ", children=" + Arrays.toString(getChildren()) +
                "}");
    }

    public void addChild(Human child){
        Human[] copyChildrens;
        if(getChildren().length == 0){
            copyChildrens = new Human[1];
            child.setFamily(this);
            copyChildrens[0] = child;
        } else {
            copyChildrens = new Human[getChildren().length+1];
            System.arraycopy(getChildren(),0,copyChildrens,0,getChildren().length);
            copyChildrens[copyChildrens.length-1] = child;
            child.setFamily(this);
        }
        setChildren(copyChildrens);
    }

// Метод удаления Child по индексу
    public boolean deleteChild (int index) {
        Human[] copyChildrens = new Human[getChildren().length-1];
        if(getChildren().length > index){
            getChildren()[index].setFamily(null);
            getChildren()[index] = null;
            for (int i = 2; i < getChildren().length-1; i++) {
                getChildren()[i-1] = getChildren()[i];
                getChildren()[i] = null;
            }
            System.arraycopy(getChildren(),0 ,copyChildrens,0 ,getChildren().length-1);
            setChildren(copyChildrens);
            return true;
        } else return false;
    }

// Метод удаления Child по экземпляру класса Human
    public void deleteChild(Human selectedChild){
        Human[] copyChildrens = new Human[getChildren().length-1];
        for (int i = 0; i < getChildren().length; i++) {
            if(getChildren()[i].equals(selectedChild)){
                System.out.printf("Deleted %s", getChildren()[i]);
                getChildren()[i].setFamily(null);
                getChildren()[i] = null;
                for (int j = i; j < getChildren().length; j++){
                    Human temp = getChildren()[i];
                    getChildren()[i] = getChildren()[i+1];
                }
                System.arraycopy(getChildren(),0 ,copyChildrens,0 ,getChildren().length-1);
                setChildren(copyChildrens);
            }
        }
    }

    public int countFamily(){
        int familySum = 0;
        if(this.getFather()!= null && this.getMother() != null){
            familySum = 2;
            if(this.getPet() != null){
                familySum = 3;
            }
            for (Human key : getChildren()) {
                familySum++;
            }
        }
        return familySum;
    }
}