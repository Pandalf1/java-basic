package com.company;

public class Main {

    public static void main(String[] args) {
        Human father = new Human("Viktor","Veresk",1970);
        Human mother = new Human("Olga","Veresk",1970);
        Human child1 = new Human("Ivan","Veresk",2000);
        Human child2 = new Human("Vlad","Veresk",1998);
        Human child3 = new Human("Ivan","Veresk",2000,90,
                new String[][]{{"Sunday, learn Java"},{"Monday, learn JavaScript"}});
        Human children[] = {child1};
        Pet cat = new Pet("Cat","Nuckles",10,20,
                new String[]{"eat","drink","sleep"});

        Family familyVeresk = new Family(mother,father,children,cat);

        System.out.println(familyVeresk);
        for (Human key:
                familyVeresk.getChildren()){
            System.out.println(key);
        }
        System.out.println(familyVeresk.getPet());
        System.out.println(familyVeresk.getMother());
        System.out.println(familyVeresk.getFather());
        System.out.println(familyVeresk.countFamily());
        familyVeresk.addChild(child2);
        for (Human key:
                familyVeresk.getChildren()){
            System.out.println(key);
        }
        System.out.println(familyVeresk);
        System.out.println(familyVeresk.countFamily());
        father.feedPet(false);

        System.out.println(familyVeresk.countFamily());
        System.out.println(familyVeresk);
        System.out.println(child1.equals(child2));
        System.out.println(child1.equals(child3));
        System.out.println(child2.equals(child3));

        familyVeresk.deleteChild(child3);

        System.out.println(familyVeresk);

        familyVeresk.addChild(child3);

        System.out.println(familyVeresk);
    };

}
