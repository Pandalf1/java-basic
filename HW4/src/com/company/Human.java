package com.company;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String schedule[][];

    static {
        System.out.printf("Загружается новый класс %s\n",Human.class.getSimpleName());
    }

    {
        System.out.printf("%s: новый экземпляр\n",this.getClass().getSimpleName());
    }

    public Human(){
        this.name = "undefined";
        this.surname = "undefined";
        this.year = 0;
        this.iq = 0;
        this.family = null;
        this.schedule = new String[0][0];
    }

    public Human(String name, String surname, int year){
        this();
        this.name = name;
        this.surname = surname;
        this.year = year;
    }


    public Human(String name, String surname, int year, int iq, String[][] schedule){
        this(name, surname, year);
        this.iq = iq;
        this.schedule = schedule;
    }


    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public String getSurname(){
        return surname;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    public void setIq(int iq) {
        if(iq <= 100 && iq >= 0){
            this.iq = iq;
        } else System.out.printf("IQ is out of range\n");
    }

    public int getIq() {
        return iq;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    @Override
    public String toString() {
        return String.format(
                "Human{" + "name=" + "'" + getName() +
                        "', surname="+getSurname()) +
                ", year=" + getYear() +
                ", iq=" + getIq() +
                ", schedule=" + Arrays.deepToString(getSchedule()) +
                "}\n";
    }

    public boolean feedPet (boolean isItTimeToFeed){
        if(family != null) {
            if (isItTimeToFeed){
                System.out.printf("Хм... покормлю ка я %s\n", family.getPet().getNickname());
                return true;
            } else {
                final Random random = new Random();
                if ( family.getPet().getTrickLevel() > random.nextInt(100)){
                    System.out.printf("Хм... покормлю ка я %s \n", family.getPet().getNickname());
                    return true;
                } else {
                    System.out.printf("Думаю, %s не голоден.\n", family.getPet().getNickname());
                    return false;
                }
            }
        } else return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return this.getYear() == human.getYear() &&
                this.getName().equals(human.getName()) &&
                this.getSurname().equals(human.getSurname()) &&
                Objects.equals(this.getFamily(), human.getFamily());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getSurname(), getYear(), getFamily());
    }
}
