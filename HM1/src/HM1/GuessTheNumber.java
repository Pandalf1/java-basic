package HM1;
import java.util.Scanner;
import java.util.Random;

public class GuessTheNumber {
    public static boolean isInteger(String s) {
        return isInteger(s,10);
    }

    public static boolean isInteger(String s, int radix) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),radix) < 0) return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        final Random random = new Random();

//        Псевдослучайное число в диапазоне [0-100], включительно;
//        int hiddenNumber = random.nextInt(101);

//        Псевдослучайное число в диапазоне[0-7], включительно;
        int hiddenNumber = random.nextInt(10);

        String questionsArray[][] = {
                {"When the Treaty of Tordesillas was signed?",
                        "When Constantinople fell?",
                        "The beginning of the Franco-Prussian war",
                        "Year of the Battle of Rorke's Drift?",
                        "When the atom was discovered?",
                        "When was the first edition of Dungeons and Dragons published?",
                        "When was The Hobbit, or There and Back again published?",
                        "When Conan the Barbarian was written?"},
                {"1494","1453","1870","1879","1911","1974","1937","1934"}
        };

        int playerAttemptsNumberList[] = new int[100];

        System.out.printf("Input your name\n");
        String name = in.nextLine();

        System.out.printf("Let the game begin!\n");

        int attempts = 0;

        do {

//            System.out.printf("Guess the number. Input your number\n");

            System.out.printf("Your question is: %s\n",questionsArray[0][hiddenNumber]);
            String playerLine;

            do {
                playerLine = in.nextLine();
            }while(!isInteger(playerLine));

            int playerNumber = Integer.parseInt(playerLine);
            int correctAnswer = Integer.parseInt(questionsArray[1][hiddenNumber]);

            if(playerNumber < correctAnswer){
                playerAttemptsNumberList[attempts] = playerNumber;

                System.out.printf("Your number is too small. Please, try again.");
                attempts++;
                continue;
            } else {
                if (playerNumber > correctAnswer){
                    playerAttemptsNumberList[attempts] = playerNumber;

                    System.out.printf("Your number is too big. Please, try again.");
                    attempts++;
                    continue;
                } else {
                    if(playerNumber == correctAnswer){

                        // Сортировка пузырьком по убыванию
                        for (int i = 0; i < playerAttemptsNumberList.length; i++) {
                            int max = playerAttemptsNumberList[i];
                            int maxId = i;
                            for (int j = i+1; j < playerAttemptsNumberList.length; j++) {
                                if (playerAttemptsNumberList[j] > max) {
                                    max = playerAttemptsNumberList[j];
                                    maxId = j;
                                }
                            }

                            int temp = playerAttemptsNumberList[i];
                            playerAttemptsNumberList[i] = max;
                            playerAttemptsNumberList[maxId] = temp;
                        }

                        System.out.printf("Congratulations,%s!\n",name);

                        if(attempts != 0){
                            System.out.printf("You numbers: ");
                            for(int i = 0; i != attempts; i++){
                                System.out.printf(" %d",playerAttemptsNumberList[i]);
                            }
                        }
                        break;
                    }
                }
            }
        }while(true);
    }
}
