package com.company;
import java.util.Scanner;
import java.util.Random;

public class AreaShooting {

//    возвращает двумерный масив Х и Y
    private static int[][] createTripleTarget(int x0, int y0){
        final Random random = new Random();
        int coordinateLine = random.nextInt(2);
        int target [][] = new int[2][3];

        if (coordinateLine == 1){
            target[1] = createTripleTarget(x0);
            for (int i = 0; i < target[0].length;i++){
                target[0][i] = y0;
            }
        } else {
            target[0] = createTripleTarget(y0);
            for (int i = 0; i < target[1].length;i++){
                target[1][i] = x0;
            }
        }

        return target;
    }

//    генерирует точки вокруг переданной координаты
    private static int[] createTripleTarget(int coordinate0){
        int target[]=new int[3];

        do {
            if (coordinate0 == 0){
                target[0] = coordinate0;
                target[1] = coordinate0 + 1;
                target[2] = coordinate0 + 2;
                break;
            } else {
                if(coordinate0 == 4){
                    target[0] = coordinate0;
                    target[1] = coordinate0 - 1;
                    target[2] = coordinate0 - 2;
                    break;
                } else {
                    target[0] = coordinate0;
                    target[1] = coordinate0 - 1;
                    target[2] = coordinate0 + 1;
                    break;
                }
            }
        }while (true);

        return target;
    }

//  перерисовывает игровое поле
    private static void renderGrid(String displayedGrid[][]){
        System.out.printf(" 0 | 1 | 2 | 3 | 4 | 5 |\n");

        for (int i = 0; i < 5; i++){
            System.out.printf(" %d |",i+1);
            for (int j = 0; j < 5; j++){
                System.out.printf("%s",displayedGrid[i][j]);
            }
            System.out.printf("\n");
        }
    }

//    выводит на экран массив, для проверки area[][]
    private static void checkArea(String area[][]){
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 3; j++){
                System.out.printf("%d ",area[i][j] + 1);
            }
            System.out.printf("\n");
        }
        System.out.printf("\n");
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        final Random random = new Random();
        int coordinateX0 = random.nextInt(5);
        int coordinateY0 = random.nextInt(5);

        int area[][] = createTripleTarget(coordinateX0,coordinateY0);

        String displayedGrid[][] = new String[5][5];

        for (int i = 0; i < 5; i++){
            for (int j = 0; j < 5; j++){
                displayedGrid[i][j] = " - |";
            }
        }
        System.out.printf("\nAll set. Get ready to rumble!\n");

        int hitsCount = 0;

        do{

            renderGrid(displayedGrid);

            if (hitsCount == 3){
                System.out.printf("You have won!");
                break;
            }

            int playerCoordinateX, playerCoordinateY;

            System.out.printf("Input row number\n");
            playerCoordinateY = in.nextInt() - 1;

            System.out.printf("Input column number\n");
            playerCoordinateX = in.nextInt() - 1;



            for (int i = 0; i < 3; i++){
                if (area[0][i] == playerCoordinateY
                        && area[1][i] == playerCoordinateX){

                    if(!displayedGrid[playerCoordinateY][playerCoordinateX].equals(" x |")){
                        displayedGrid[playerCoordinateY][playerCoordinateX] = " x |";
                        hitsCount++;
                        break;
                    }

                } else {

                    if (area[0][i] != playerCoordinateY
                            && area[1][i] != playerCoordinateX){
                        displayedGrid[playerCoordinateY][playerCoordinateX] = " * |";
                        break;
                    }
                }
            }
        }while (true);
    }
}
