package com.company;

import java.util.Scanner;

public class Scedule {
//  Возвращает команду change или reschedule
    private static String[] takeCommandFromString(String inputString){
        String outputArray[] = inputString.trim().split(" ",2);
        return outputArray;
    }
//  Проверяет соответствует введённая строка дню в scedule[][]
    private  static  boolean isInScedule(String inputString, String scedule[][]){
        for (int i = 0; i < scedule.length; i++){
            if(inputString.trim().equalsIgnoreCase(scedule[i][0]) == true){
                return true;
            }
        }
        return false;
    }

//  Проверяет есть ли в ведённой команде change или reschedule
    private static boolean isChange(String inputString, String scedule[][]){
        String inputArray[] = inputString.trim().split(" ",2);
        if(inputArray[0].equalsIgnoreCase("change")
                || inputArray[0].equalsIgnoreCase("reschedule")){
            if(isInScedule(inputArray[1],scedule) == true){
                return true;
            }
        }
        return false;
    }

//  Приводит строку к формату [A-Z]{1}[a-z]+
    private static  String newInputFormat(String inputString){
        char[] inputArray = inputString.trim()
                .toLowerCase()
                .toCharArray();
        inputArray[0] = Character.toUpperCase(inputArray[0]);
        String outputString = new String(inputArray);

        return outputString;
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[][] scedule = new String[7][2];
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to courses";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "practice coding in java";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to courses";
        scedule[5][0] = "Friday";
        scedule[5][1] = "practice coding in java";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "practice coding in javascript";

        String selectedDayOfTheWeek = "";
        String day = "";
        outerloop:
        do{
            System.out.printf("Please, input the day of the week:\n");
            do {
                selectedDayOfTheWeek = in.nextLine();

                if(isInScedule(selectedDayOfTheWeek,scedule) == true ||
                        selectedDayOfTheWeek.equalsIgnoreCase("exit")){
                    break;
                } else {
                    if(isChange(selectedDayOfTheWeek,scedule) == true){
                        day = takeCommandFromString(selectedDayOfTheWeek)[1];
                        selectedDayOfTheWeek = takeCommandFromString(selectedDayOfTheWeek)[0];
                        break;
                    } else System.out.printf("Sorry, I don't understand you, please try again.\n");
                }

            }while (true);

            switch (newInputFormat(selectedDayOfTheWeek)){
                case "Sunday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[0][1]);
                    break;
                case "Monday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[1][1]);
                    break;
                case "Tuesday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[2][1]);
                    break;
                case "Wednesday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[3][1]);
                    break;
                case "Thursday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[4][1]);
                    break;
                case "Friday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[5][1]);
                    break;
                case "Saturday":
                    System.out.printf("Your tasks for %s: %s \n",
                            newInputFormat(selectedDayOfTheWeek),
                            scedule[6][1]);
                    break;
                case "Change":
                case "Reschedule":
                    System.out.printf("Please, input new tasks for %s\n", newInputFormat(day));
                    String task = in.nextLine();
                    for(int i = 0; i < scedule.length; i++){
                        if(day.equalsIgnoreCase(scedule[i][0])){
                            scedule[i][1] = task;
                        }
                    }
                    break;
                case "Exit":
                    break outerloop;
            }
        }while (true);
    }
}
